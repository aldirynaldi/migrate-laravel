@extends('adminlte.master')

@section('contents')
    <div class="card card-primary my-4 mx-4">
        <div class="card-header">
            <h3 class="card-title">Create Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/cast" method="POST">
            @csrf
            <div class="card-body">
                
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" id="name" name="nama" value="{{ old('nama', '') }}" placeholder="Enter Name">
                    
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" placeholder="Enter Umur">

                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <!-- textarea -->
                <div class="form-group">
                    <label>Bio</label>
                    <textarea class="form-control" rows="3" name="bio" placeholder="Enter ..."></textarea>

                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
@endsection