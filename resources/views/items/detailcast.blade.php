@extends('adminlte.master')

@section('contents')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Detail Cast</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
    </section>
  
      <!-- Main content -->
    <section class="content">
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title"> <b>{{ $detailCast->nama }}</b></h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">

            {{--  --}}

            <p>Umur : {{ $detailCast->umur }}</p>
            <h4>Bio : </h4>
            <p>{{ $detailCast->bio }}</p>

            {{--  --}}

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
  
    </section>
    <!-- /.content -->
@endsection