<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function tampil() {
        $getCasts = DB::table('cast')->get(); //select * from cast
        //dd($getCast->all());
        return view('items.tampilcast', compact('getCasts'));
    }

    public function create() {
        return view('items.createcast');
    }

    public function store(Request $request) {
        //dd($request->all());

        $validatedData = $request->validate(
            ['nama' => 'required', 'unique:posts', 'max:255',
            'umur' => 'required', 'max:100',
            'bio' => 'required',
            ]
        );


        $query = DB::table('cast')->insert(
            ['nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
            ]
        );
        return redirect('/cast')->with('success', 'Cast Berhasil disimpan!');
    }

    public function detail($id) {
        $detailCast = DB::table('cast')->where('id', $id)->first();

        //dd($cast);

        return view('items.detailcast', compact('detailCast'));

    }

    public function edit($id) {
        $cast = DB::table('cast')->where('id', $id)->first();

        //dd($cast);

        return view('items.editcast', compact('cast'));

    }

    public function update($id, Request $request) {
        
        $validatedData = $request->validate(
            ['nama' => 'required', 'unique:posts', 'max:255',
            'umur' => 'required', 'max:100',
            'bio' => 'required',
            ]
        );

        $query = DB::table('cast')->where('id', $id)->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        return redirect('/cast')->with('success', 'Update Berhasil!');
    }

    public function destroy($id) {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Delete Berhasil!');
    }
}
