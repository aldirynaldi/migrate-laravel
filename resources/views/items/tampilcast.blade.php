@extends('adminlte.master')

@section('contents')
    
<!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Table</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
  
        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">table</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">

            @if (session('success'))
                <div class="alert alert-success">
                  {{ session('success') }}
                </div>
            @endif

            <a href="/cast/create" class="btn btn-info mb-4">Create Cast</a>

            {{--  --}}
            <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th style="width: 40px">Label</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($getCasts as $key => $cast)
                      <tr>
                          <td>{{ $key+1 }}</td>
                          <td>{{ $cast->nama }}</td>
                          <td>{{ $cast->umur }}</td>
                          <td align="center">
                            <a href="/cast/{{$cast->id}}" class="btn btn-info mb-4">detail</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-light mb-4"><i class="fas fa-edit"></i></a>
                            <form action="/cast/{{$cast->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <input type="submit" value="delete" class="btn btn-danger">
                            </form>
                          </td>
                      </tr>
                  @empty
                      <tr>
                          <td colspan="3" align="center">Cast Empty</td>
                      </tr>
                  @endforelse
                </tbody>
              </table>
            {{--  --}}


          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
  
      </section>
      <!-- /.content -->

@endsection